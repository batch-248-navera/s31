/*

What directive is used by Node.js in loading the modules it needs?
- Require directive

Which method is used to create server in node JS?
- createServer() method

What is the method of the http object responsible for creating a server using Node.js?
- http.createServer() method

What method of the response object allows us to set status codes and content types?
- writeHead() method

Where will console.log() output its contents when run in Node.js?
- Terminal

What property of the request object contains the address's endpoint?
- url


*/