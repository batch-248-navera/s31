const http = require('http');

const hostname = 'localhost';
const port = 3000;

const server = http.createServer((req, res) => {

    if(req.url == '/login'){

      res.statusCode = 200;
      res.setHeader('Content-Type', 'text/plain');
      res.end('This is the login page');
      
    }

  
    else{
       
            res.statusCode = 404;
            res.setHeader('Content-Type', 'text/plain');
            res.end("404 Page not found!");
          
        
    }
  
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);

});

